using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pausa : MonoBehaviour
{
    private bool ispausa = false;

    public GameObject menu_pausa;
    // Start is called before the first frame update
    void Start()
    {
        menu_pausa.SetActive(false);   
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel") && ispausa == false) 
        {
            ispausa = true; 
            menu_pausa.SetActive(true);
            Time.timeScale = 0f;
            
        }
        else if (Input.GetButtonDown("Cancel") && ispausa == true)
        {

            btn_continuar();
        }
    }
    public void btn_continuar() 
    {
        ispausa = false;
        menu_pausa.SetActive(false);
        Time.timeScale = 1f;

    }
}
