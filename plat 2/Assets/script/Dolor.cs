using UnityEngine;
using UnityEngine.SceneManagement;

public class Dolor : MonoBehaviour
{
    private GameObject _gameObject;
    private Rigidbody2D _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _gameObject = GameObject.FindGameObjectWithTag("Jugador");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Dolor"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
