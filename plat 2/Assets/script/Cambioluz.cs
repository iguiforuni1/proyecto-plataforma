
using UnityEngine;

namespace MyNamespace
{
    public class cambioluz : MonoBehaviour
    {
        public GameObject[] darkObjects;
        public GameObject[] lightObjects;

        private bool iscambio = false;

        void Start()
        {
            // Encuentra todos los objetos con la etiqueta "obstaculos_luz" y los desactiva
           // lightObjects = GameObject.FindGameObjectsWithTag("obstaculos_luz");
           // darkObjects = GameObject.FindGameObjectsWithTag("obstaculos_osc");
            foreach (GameObject lightObject in lightObjects)
            {
                lightObject.SetActive(false);
            }
        }

        void Update()
        {
            if (Input.GetButtonDown("Switch"))
            {
                Debug.Log("works");

                if (iscambio == false)
                {
                    iscambio = true;
                    Debug.Log("obj_negros fuera, obj_blancos dentro");
                    foreach (GameObject darkObject in darkObjects)
                    {
                        darkObject.SetActive(false);
                    }
                    foreach (GameObject lightObject in lightObjects)
                    {
                        lightObject.SetActive(true);
                        Debug.Log("Estado del objeto claro: " + lightObject.activeSelf);
                    }
                }
                else
                {
                    iscambio = false ;
                    Debug.Log("obj_negros dentro, obj_blancos fuera");
                    foreach (GameObject darkObject in darkObjects)
                    {
                        darkObject.SetActive(true);
                    }
                    foreach (GameObject lightObject in lightObjects)
                    {
                        lightObject.SetActive(false);
                    }
                }
            }
        }
    }
}