using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    public float jumpForce = 5f;
    public float dashSpeed = 20f;

    private bool isJumping = false;
    private bool isDashing = false;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float moveX = Input.GetAxis("Horizontal");

        if (isDashing)
        {
            rb.velocity = new Vector2(dashSpeed * moveX, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(speed * moveX, rb.velocity.y);
        }

        if (Input.GetButtonDown("Jump") && !isJumping)
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
            isJumping = true;
        }

        if (Input.GetButtonDown("Dash"))
        {
            isDashing = true;
        }

        if (Input.GetButtonUp("Dash"))
        {
            isDashing = false;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
        }
    }
}
