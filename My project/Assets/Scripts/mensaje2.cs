using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mensaje2 : MonoBehaviour
{
    private GameObject _gameObject;
    private Rigidbody2D _rb;
    private GameObject Mensaje; // Declaraci�n de la variable

    void Start() // 'S' en may�scula
    {
        Mensaje = GameObject.FindGameObjectWithTag("Mensaje2"); // Asignaci�n de valor
        Mensaje.SetActive(false);
    }

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _gameObject = GameObject.FindGameObjectWithTag("Jugador");
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Mensaje2"))
        {
            Mensaje.SetActive(true);
        }
    }
}