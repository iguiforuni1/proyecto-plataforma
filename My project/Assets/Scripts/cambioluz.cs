using UnityEngine;

namespace MyNamespace
{
    public class cambioluz : MonoBehaviour
    {
        private GameObject[] darkObjects;
        private GameObject[] lightObjects;

        void Start()
        {
            // Encuentra todos los objetos con la etiqueta "obstaculos_luz" y los desactiva
            lightObjects = GameObject.FindGameObjectsWithTag("obstaculos_luz");
            darkObjects = GameObject.FindGameObjectsWithTag("obstaculos_osc");
            foreach (GameObject lightObject in lightObjects)
            {
                lightObject.SetActive(false);
            }
        }

        void Update()
        {
            if (Input.GetButtonDown("Switch"))
            {
                Debug.Log("tu mama come brocoli");

                if (darkObjects[0].activeSelf)
                {
                    Debug.Log("negros fuera, blancos dentro");
                    foreach (GameObject darkObject in darkObjects)
                    {
                        darkObject.SetActive(false);
                    }
                    foreach (GameObject lightObject in lightObjects)
                    {
                        lightObject.SetActive(true);
                    }
                }
                else
                {
                    Debug.Log("negros dentro, blancos fuera");
                    foreach (GameObject darkObject in darkObjects)
                    {
                        darkObject.SetActive(true);
                    }
                    foreach (GameObject lightObject in lightObjects)
                    {
                        lightObject.SetActive(false);
                    }
                }
            }
        }
    }
}