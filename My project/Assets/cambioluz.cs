using UnityEngine;
public class cambioluz : Monobehaviour
{
    void Update()
    {
        if (Input.GetButtonDown("Switch"))
        {
            GameObject[] darkObjects = GameObject.FindGameObjectsWithTag("obstaculos_osc");
            GameObject[] lightObjects = GameObject.FindGameObjectsWithTag("obstaculos_luz");

            foreach (GameObject obj in darkObjects)
            {
                obj.SetActive(false);
            }

            foreach (GameObject obj in lightObjects)
            {
                obj.SetActive(true);
            }
        }
    }
}
